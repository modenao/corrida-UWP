﻿using Corrida.Misc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace Corrida.Pages
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Lobby : Page
    {
        #region Constructor
        public Lobby()
        {
            this.InitializeComponent();

            //Binding players to list
            players = client.Players;
            PlayersList.ItemsSource = players;

            ListenForStart();
        }
        #endregion

        //The players list (Declared a new one for commodity)
        Players players;

        //The client's instance
        private NAOClient client = NAOClient.GetInstance();

        #region Listen
        /// <summary>
        /// Listens for the nao's messages 
        /// </summary>
        private async void ListenForStart ()
        {
            try
            {
                //Reading the message 
                do
                {
                    //Waiting for a message from the NAO
                    string message = await client.ReceiveTcpAsync();

                    //The message args
                    var args = message.Split(' ').ToList();

                    var command = args[0];  //<--The message command
                    args.RemoveAt(0);       //<--Keeping only the args (Ex. player names)


                    switch (command)
                    {
                        //Getting the players list
                        case "players":
                            args.ForEach(
                                name => players.Add(new Player(name)));
                            break;

                        //Adding new player
                        case "player_join":
                            players.Add(
                                new Player(args[0]));
                            break;

                        //Removing player
                        case "player_quit":
                            players.Remove(
                                players.First(pl => pl.Name == args[0]));
                            break;

                        //Starting game
                        case "performance_start":
                            App.Frame.Navigate(typeof(Game), args[0]);
                            return;
                    }
                }
                while (true);
            }
            catch (Exception exc)
            {
                await new EndGameDialog(GameEndedBy.Error, exc.Message).ShowAsync();
            }
        }
        #endregion

        #region Start button
        private async void StartButton_Click(object sender, RoutedEventArgs e)
        {
            await client.SendTcpAsync("request_start");
        }
        #endregion
    }
}
