﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Corrida.Misc;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Diagnostics;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace Corrida.Pages
{
    public sealed partial class Login : Page
    {
        //NAOClient instance
        private NAOClient client = NAOClient.GetInstance();

        public Login() => this.InitializeComponent();
        
        //NaoAddress
        private IPAddress NAOAddress;    bool IsIpValid   = false;

        //Tells if the client is trying to connect
        private bool IsConnecting = false;

        #region ConnectButton 
        private async void ConnectButton_Tapped(object sender, RoutedEventArgs e)
        {
            IsConnecting = true;
            try
            {
                //Seeing if the ip is valid
                if (!IsIpValid)
                    ConnectionState_TxtBlock.Text = "Utilizzando i valori di default\n";
                else
                    ConnectionState_TxtBlock.Text = "";

                ConnectionState_TxtBlock.Text += "Connessione...\n";


                #region UI stuff 
                PageStackPanel.Opacity = 0.5;
                ConnectButton.IsEnabled = false;
                ProgressRing_.IsActive = true;
                #endregion


                //Connecting the NAOClient to the NAO
                await
                    client.RunConnectionAsync(IsIpValid ? NAOAddress : null);

                App.Frame.Navigate(typeof(WriteName));
            }
            catch (Exception exc)
            {
                //Communicating failed connection attempt
                ConnectionState_TxtBlock.Text =
                    "Non posso connettermi al NAO\nRicontrolla i parametri inseriti o assicurati che il NAO sia online\n";

                //Writing the error on debug line
                Debug.WriteLine(exc.Message);

                #region Same UI stuff
                PageStackPanel.Opacity = 1;
                ConnectButton.IsEnabled = true;
                ProgressRing_.IsActive = false;
                #endregion
            }

            IsConnecting = false;
        }
        #endregion

        #region Validating IP
        private void Address_TextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            //If enter is pressed...
            if (e.Key == Windows.System.VirtualKey.Enter && !IsConnecting)
            {
                ConnectButton_Tapped(null, null);
                return;
            }

            //Validating the address
            if (Address_TextBox.Text == "localhost")
                NAOAddress = IPAddress.Loopback;

            else if (IPAddress.TryParse(Address_TextBox.Text, out IPAddress addr))
                NAOAddress = addr;
            else
            {
                IsIpValid = false;

                if (Address_TextBox.Text == "")
                    Address_TextBox.BorderBrush = new SolidColorBrush(Colors.Gray);
                else
                    Address_TextBox.BorderBrush = new SolidColorBrush(Colors.Red);
                return;
            }

            IsIpValid = true;
            Address_TextBox.BorderBrush = new SolidColorBrush(Colors.Green);
        }
        #endregion
    }
}
