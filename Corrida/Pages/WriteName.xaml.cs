﻿using Corrida.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace Corrida.Pages
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class WriteName : Page
    {
        public WriteName() => this.InitializeComponent();

        //Getting NAOClient instance
        NAOClient client = NAOClient.GetInstance();

        //Tells if the StartButton method is running
        private bool IsStartBusy = false;

        #region Start button
        private async void StartButton_Tapped(object sender, RoutedEventArgs e)
        {
            IsStartBusy = true;

            try
            {
                Error_TextBlock.Text = "";

                //Validating name
                if (MyName_TextBox.Text == "" || MyName_TextBox.Text.Split(' ').Length > 2)
                {
                    Error_TextBlock.Text = "Nome errato";
                    return;
                }


                //Sending name
                await client.SendTcpAsync("name " + MyName_TextBox.Text);


                //awaiting response
                if (await client.ReceiveTcpAsync() == "name_response ok")
                {
                    client.Me = new Player(MyName_TextBox.Text);
                    App.Frame.Navigate(typeof(Lobby));
                }
                else
                    Error_TextBlock.Text = "Nome già preso";
            }
            catch (Exception exc)
            {
                await new EndGameDialog(GameEndedBy.Error, exc.Message).ShowAsync();
            }

            IsStartBusy = false;
        }
        #endregion

        //Connecting if pressing start inside text box
        private void MyName_TextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter && !IsStartBusy)
                StartButton_Tapped(null, null);
        }
    }
}
