﻿using Corrida.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace Corrida.Pages
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Game : Page
    {
        #region OnNavigatedTo - Checking first player
        /// <summary>
        /// Checks who's the first player
        /// </summary>
        /// <param name="e">Contains the name of the first player</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            currentPlayer = e.Parameter as string;

            if (currentPlayer == client.Me && client.currentRound == 2)
                CurrentPlayer.Text = "E' il tuo turno!";
            else if (client.currentRound == 2)
                CurrentPlayer.Text = "E' il turno di " + currentPlayer;
        }
        #endregion

        #region Constructor
        public Game()
        {
            this.InitializeComponent();

            //Updating round
            CurrentRound.Text += client.currentRound++;
            
            //Starting the listening
            Listen();
        }
        #endregion

        //Client's instance
        NAOClient client = NAOClient.GetInstance();

        private string currentPlayer;   //<--The current player


        #region Listen
        /// <summary>
        /// Listens for incoming messages from the NAO and
        /// controls the application behavior accordingly
        /// </summary>
        private async void Listen()
        {
            JokeEndButton.Visibility = Visibility.Collapsed;
           
            try
            {
                do
                {
                    //The message sent by the NAO
                    string message = await client.ReceiveTcpAsync();
                    
                    var args = message.Split(' ');  //<--The message args
                    var command = args[0];          //<--The command

                    //The command's argument, if any 
                    //(Ex. a player's name)
                    string commArg = default;
                    if (args.Length == 2)
                        commArg = args[1];
                    
                    switch (command)
                    {
                        //Getting current player
                        case "performance_start":
                            currentPlayer = commArg;

                            if (currentPlayer == client.Me.Name)
                                CurrentPlayer.Text = "E' il tuo turno!";
                            else
                                CurrentPlayer.Text = "E' il turno di " + currentPlayer;
                            break;

                        //Getting current theme
                        case "performance_theme":
                            CurrentTheme.Text = "Il tema è: " + commArg;
                            if (commArg == "battuta" && currentPlayer == client.Me.Name)
                                JokeEndButton.Visibility = Visibility.Visible;
                            else
                                JokeEndButton.Visibility = Visibility.Collapsed;
                            break;

                        //Starting the rating phase
                        case "rate_start":
                            await new RatingDialog(currentPlayer).ShowAsync();
                            return;

                        //Ending the game
                        case "game_end":
                            await new EndGameDialog(GameEndedBy.Nao).ShowAsync();
                            return;
                    }
                }
                while (true);
            }
            catch (Exception exc)
            {
                await new EndGameDialog(GameEndedBy.Error, exc.Message).ShowAsync();
            }
        }
        #endregion

        #region Joke end button
        private async void JokeEndButton_Click(object sender, RoutedEventArgs e)
        {
            await client.SendTcpAsync("end_joke");
            JokeEndButton.IsEnabled = false;
        }
        #endregion
    }
}
