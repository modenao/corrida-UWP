﻿using Corrida.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Finestra di dialogo contenuto è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace Corrida.Misc
{
    public sealed partial class RatingDialog : ContentDialog
    {
        #region Constructor
        public RatingDialog(string currentPlayer)
        {
            this.InitializeComponent();

            this.currentPlayer = currentPlayer;

            //Checking if you can vote
            if (currentPlayer == client.Me)
            {
                Rating_StackPanel.Visibility = Visibility.Collapsed;
                NotRating_TxtBlock.Visibility = Visibility.Visible;
            }
            
            ListenForEnd();
        }
        #endregion


        #region ListenForEnd
        private async void ListenForEnd()
        {
            try
            {
                //Receiving the message
                string message;
                do
                    message = await client.ReceiveTcpAsync();
                while (!message.Contains("rate_end") || message.Length <= 8);

                var players = client.Players;

                //Updating player score
                players.First(
                    pl => pl.Name == currentPlayer).Score = message.Split(' ')[1];


                App.Frame.Navigate(typeof(Game));
                Hide();
            }
            catch (Exception exc)
            {
                await new EndGameDialog(GameEndedBy.Error, exc.Message).ShowAsync();
            }
        }
        #endregion


        //Current player
        string currentPlayer;

        //Client's instance
        NAOClient client = NAOClient.GetInstance();

        double score;
        private void RatingControl_ValueChanged(RatingControl sender, object args)
        {
            //Enable button
            if (!RateButton.IsEnabled) RateButton.IsEnabled = true;

            //Setting the score
            score = sender.Value;
        }

        private async void RateButton_Click(object sender, RoutedEventArgs e)
        {
            //Sending the score
            await client.SendTcpAsync("rate " + score);

            RateButton.IsEnabled = false;
        }
    }
}
