﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Corrida.Misc
{
    /// <summary>
    /// Tells if the game has been ended because 
    /// the NAO disconnected or because the NAO 
    /// sent the "game_end" message
    /// </summary>
    public enum GameEndedBy
    {
        Error,
        Nao
    }

    class NAOClient
    {
        
    #region NAOClient instance

        private static NAOClient Instance = null;
        
        /// <summary>
        /// The <see cref="NAOClient"/> current instance
        /// </summary>
        public static NAOClient GetInstance()
        {
            Instance =
                Instance ?? new NAOClient();

            return Instance;
        }

        /// <summary>
        /// Re-instantiates the <see cref="NAOClient"/>
        /// </summary>
        public static void ReInstantiate()
        {
            Instance = new NAOClient();
        }

    #endregion


    #region Private fields

        private IPAddress   _naoAddress;
        private int         _naoPort;

        private Socket      _tcpSocket;

        private UdpClient   _udpClient;

    #endregion

    #region Properties

        /// <summary>
        /// The NAO's IPAddress
        /// </summary>
        public IPAddress    NAOAddress      { get => _naoAddress; }

        /// <summary>
        /// The NAO's port
        /// </summary>
        public int          NAOPort         { get => _naoPort; }

        /// <summary>
        /// The <see cref="System.Net.Sockets.TcpClient"/> 
        /// that the <see cref="NAOClient"/> uses to communicate
        /// </summary>
        public Socket       TcpSocket       { get => _tcpSocket; }

        /// <summary>
        /// The <see cref="System.Net.Sockets.UdpClient"/> that 
        /// the <see cref="NAOClient"/> uses to communicate
        /// </summary>
        public UdpClient    UdpClient       { get => _udpClient; }

        /// <summary>
        /// Tells wheter the <see cref="TcpSocket"/> is connected or not
        /// </summary>
        public bool         Connected       { get => _tcpSocket.Connected; }

        #endregion


    #region Public fields

        public Players Players;
        public Player  Me;

        public int currentRound = 1;

    #endregion


    #region Constructor

        private NAOClient()
        {
            //Instantiating Players list
            this.Players = new Players();

            //Instantiating the Tcp Socket
            _tcpSocket = new Socket(
                SocketType.Stream, ProtocolType.Tcp);
        }

    #endregion


    #region Udp consts

        private const string MAGIC_REQUEST = "corrida_discovery";
        private const string MAGIC_RESPONSE = "corrida_response";
        
        private readonly IPEndPoint DEFAULT_NAO_EP = new IPEndPoint(IPAddress.Broadcast, 25565);

    #endregion

    #region Run Connection

        /// <summary>
        /// Connects to the Nao
        /// </summary>
        /// <param name="endPoint">
        ///     The <see cref="IPEndPoint"/> the UdpClient connects to (default is null, and uses default port and ip)
        /// </param>
        /// <param name="timeoutTime">The timeout time in milliseconds (default is 5000)</param>
        public async Task RunConnectionAsync (IPAddress address = null, int timeoutTime = 3000)
        {
            //Instantiating the udpClient
            _udpClient = new UdpClient();

            //Sending UdpRequest async or directly connecting
            if (address == null)
            {
                //Sending the message
                byte[] dGram = Encoding.UTF8.GetBytes(MAGIC_REQUEST);
                _udpClient.Send(dGram, dGram.Length, DEFAULT_NAO_EP);
            }
            else
            {
                _naoAddress  = address;
                _naoPort     = DEFAULT_NAO_EP.Port;

                await ConnectTcpAsync(_naoAddress, _naoPort);
                return;
            }


            #region Setting timer to avoid infinite loop
            var timeout = 
                new System.Timers.Timer(timeoutTime) { AutoReset = false };

            timeout.Elapsed += (s, e) => {
                _udpClient.Close();
                _udpClient = new UdpClient();
            };
            timeout.Start();
            #endregion

            //Waiting for correct response
            UdpReceiveResult result;
            do
                result = await _udpClient.ReceiveAsync();
            while (Encoding.UTF8.GetString(result.Buffer) != MAGIC_RESPONSE);

            //Closing the Udp socket
            _udpClient.Close();


            //Setting address and port
            _naoAddress  = result.RemoteEndPoint.Address;
            _naoPort     = result.RemoteEndPoint.Port;

            //Connecting the Tcp socket
            await ConnectTcpAsync(_naoAddress, _naoPort);
        }

        #endregion


    #region Tcp methods

        #region Connect Tcp async
        public async Task ConnectTcpAsync(IPAddress address, int port)
        {
            await TcpSocket.ConnectAsync(address, port);
        }

        #endregion
        
        #region Send Tcp async
        /// <summary>
        /// Sends a message to a connected Tcp socket
        /// </summary>
        /// <param name="message">The message</param>
        /// <returns></returns>
        public async Task SendTcpAsync(string message)
        {
            //Getting the buffer
            byte[] buffer = 
                Encoding.UTF8.GetBytes(message + '\n');

            //Sending the buffer
            await _tcpSocket.SendAsync(buffer, SocketFlags.None);
        }
        #endregion
        
        #region Receive Tcp async
        /// <summary>
        /// Receives a message as an asyncronous operation
        /// </summary>
        /// <param name="bufferLenght">The lenght of the buffer that will hold the message</param>
        public async Task<string> ReceiveTcpAsync (int bufferLenght = 256)
        {
            //Buffer that will hold the message
            byte[] buffer = new byte[256];

            //Receiving the message
            await TcpSocket.ReceiveAsync(buffer, SocketFlags.None);

            //Returning the message
            return
                Encoding.UTF8.GetString(buffer.Where(ch => ch != default && ch != '\n').ToArray());
        }
        #endregion

    #endregion

    }
}