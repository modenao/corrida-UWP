﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Controllo utente è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234236

namespace Corrida.Misc
{
    public sealed partial class IconButton : UserControl
    {

        #region First icon 
        public string FirstIcon
        {
            get => _FirstIcon.Glyph;
            set => _FirstIcon.Glyph = value;
        }
        #endregion

        #region Text
        public string Text
        {
            get => _Text.Text;
            set => _Text.Text = value;
        }
        #endregion

        #region Second icon 
        public string SecondIcon
        {
            get => _SecondIcon.Glyph;
            set => _SecondIcon.Glyph = value;
        }
        #endregion


        #region Constructor
        public IconButton()
        {
            this.InitializeComponent();
        }
        #endregion


        public event RoutedEventHandler Click;
        private void Button_Click(object sender, RoutedEventArgs e) => Click(sender, e);
    }
}
