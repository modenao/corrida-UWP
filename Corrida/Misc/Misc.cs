﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;

namespace Corrida.Misc
{
    public class FloatToStringConverter : IValueConverter
    {
        #region Constructor
        public FloatToStringConverter()
        {
            // Default value for DigitsCount
            DigitsCount = 7;
        }
        #endregion


        #region Convert
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return null;

            float fValue = (float)value;

            return fValue;
            
        }

        #endregion


        #region Convert back
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return null;

            double? result = null;

            try
            {
                result = System.Convert.ToSingle(value);
            }
            catch
            {
            }

            return result.HasValue ? (object)result.Value : DependencyProperty.UnsetValue;
        }
        #endregion

        public int DigitsCount { get; set; }
    }

    class Misc
    {
    }
}
