﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace Corrida.Misc
{
    class Players : ObservableCollection<Player>{}

    class Player : INotifyPropertyChanged
    {
        #region Cup color
        public enum Cup
        {
            Gold,
            Silver,
            Bronze,
            Other
        }
        
        public void AssignCup(Cup cupColor)
        {
            switch (cupColor)
            {
                case Cup.Gold: CupColor = new SolidColorBrush(Colors.Gold);
                    break;
                case Cup.Silver: CupColor = new SolidColorBrush(Colors.Silver);
                    break;
                case Cup.Bronze: CupColor = new SolidColorBrush(Colors.SandyBrown);
                    break;
                case Cup.Other: CupColor = new SolidColorBrush(Colors.White);
                    break;
            }
        }

        private SolidColorBrush _cupColor;
        public SolidColorBrush CupColor
        {
            get => _cupColor;
            set { _cupColor = value; NotifyPropertyChanged("CupColor"); }
        }
        #endregion

        #region Player name
        private string _name;
        public string Name
        {
            get => _name;
            set { _name = value; NotifyPropertyChanged("Name"); }
        }
        #endregion

        #region Player score
        private string  _score;
        public string Score
        {
            get => _score;
            set { _score = value; NotifyPropertyChanged("Score"); }
        }
        #endregion


        public Player(string name)
        {
            Name = name;
            Score = default;
            CupColor = new SolidColorBrush(Colors.White);
        }

        //Notifying the ICollection obj that a change has been made
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        #region ToString implicit conversion
        public static implicit operator string (Player pl) => pl.Name;
        #endregion

    }
}
