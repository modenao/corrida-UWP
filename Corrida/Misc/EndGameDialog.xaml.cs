﻿using Corrida.Pages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Finestra di dialogo contenuto è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace Corrida.Misc
{
    /// <summary>
    /// Shows the final leaderboard
    /// </summary>
    public sealed partial class EndGameDialog : ContentDialog
    {
        //NAOClient's instance
        NAOClient client = NAOClient.GetInstance();

        #region Constructor
        /// <param name="endBy">Tells how the game ended</param>
        /// <param name="errorMsg">An additional error message, for debug</param>
        public EndGameDialog(GameEndedBy endBy, string errorMsg = "Game ended correctly")
        {
            this.InitializeComponent();

            //Telling why the game ended
            if (endBy == GameEndedBy.Error)
                Title = "Il NAO si è disconnesso, partita finita!";
            else
                Title = "Partita finita!";


            //Shows the error, if any (for debug)
            Debug.WriteLine(errorMsg); 

            //Writing the final result (only if the game has been played)
            var players = NAOClient.GetInstance().Players;
            if (players.Count > 0)
            {
                //Reordering the players
                var ordPlayers =
                    players.OrderByDescending(pl => pl.Score).ToList();

                players.Clear();
                foreach (Player p in ordPlayers)
                    client.Players.Add(p);

                //Assigning the cups
                players[0].AssignCup(Player.Cup.Gold);
                players[1].AssignCup(Player.Cup.Silver);
                if (players.Count > 2)
                    players[2].AssignCup(Player.Cup.Bronze);

                PlayersList.Header = "Risultato:";
                PlayersList.ItemsSource = players;
            }
            else
                UnexpectedError.Text = 
                    "Si è verificato un errore inaspettato, come sta il tuo NAO?";
        }
        #endregion

        //Going back to the welcome page
        private void ContentDialog_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            NAOClient.ReInstantiate();
            App.Frame.Navigate(typeof(Welcome));
        }
    }
}
